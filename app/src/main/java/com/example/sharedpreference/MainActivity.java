package com.example.sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sharedpreference.Services.SessionManager;
//masin cvklasasmnjmnsadlksdnoahnsoihsiodnkoajsndkjwlqasdsasdasdsa
public class MainActivity extends AppCompatActivity {
    SessionManager session;
    Boolean isRemember,isrem;
    String emailVal,nameVal;

    CheckBox checkBox;
    Button logBtn;
    EditText email,name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session=new SessionManager(this);
        isrem=session.getIsRemember();
        //check in shared pref is isRemenber is true or false
        Toast.makeText(this,session.getIsRemember().toString(),Toast.LENGTH_LONG).show();
        if(isrem==true){
            //already remembered
            gotoDashboard();
        }else{
            //not rememberd

          logBtn=findViewById(R.id.logBtn);
          logBtn.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
//
                  email=findViewById(R.id.emailtxt);
                  emailVal=email.getText().toString();

                  name=findViewById(R.id.nametxt);
                  nameVal=name.getText().toString();

                  checkBox=findViewById(R.id.checkBox);
                  if(checkBox.isChecked()){
                      isRemember=true;
                  }else {
                      isRemember=false;
                  }
//                  data wirting in sahared opreed
                  session.setFullName(nameVal);
                  session.setEMAIL(emailVal);
                  session.setIsRemember(isRemember);
                  Toast.makeText(MainActivity.this, "Value is wriiten in sharedPref", Toast.LENGTH_SHORT).show();
                  //dashboard
                  gotoDashboard();
              }
          });
        }
    }
    public void gotoDashboard(){
        Intent in = new Intent(this,Dashboard.class);
        startActivity(in);
    }
}