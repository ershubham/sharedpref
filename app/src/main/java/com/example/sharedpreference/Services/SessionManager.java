package com.example.sharedpreference.Services;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {
     SharedPreferences SharePref;
     SharedPreferences.Editor editor;
    Context _context;

     int PRIVATE_MODE=0;
    private  static final String PREF_NAME="LoginApp";
    //variables hoold
    private static final String FULL_NAME="SHUBHAM";
    private static final String EMAIL="abkc@gmail.copm";
    private static final Boolean IS_REMEMBER=false;


    public SessionManager(Context context){
        this._context=context;
        SharePref= _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = SharePref.edit();
    }

    public void setFullName(String fullName) {
        editor.putString(FULL_NAME,fullName);
        editor.commit();
    }

    public void setEMAIL(String EMAIL) {
       editor.putString(EMAIL,EMAIL);
        editor.commit();
    }

    public void setIsRemember(Boolean isRemember) {
        editor.putBoolean(String.valueOf(IS_REMEMBER),isRemember);
        editor.commit();
    }

    public Boolean getIsRemember() {
        return SharePref.getBoolean(String.valueOf(IS_REMEMBER),false);
    }

    public  String getEMAIL() {
        return SharePref.getString(EMAIL,"");
    }

    public  String getFullName() {
        return SharePref.getString(FULL_NAME,"");
    }
}
