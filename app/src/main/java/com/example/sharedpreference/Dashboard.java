package com.example.sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharedpreference.Services.SessionManager;

public class Dashboard extends AppCompatActivity {
    SessionManager session;
    TextView resultText;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        session=new SessionManager(this);
        Toast.makeText(this, session.getFullName(), Toast.LENGTH_SHORT).show();
        resultText=findViewById(R.id.resultview);
        resultText.setText(session.getFullName());
        button=findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.setIsRemember(false);
                session.setEMAIL("");
                session.setFullName("");
                Toast.makeText(Dashboard.this,"Logged oUt ",Toast.LENGTH_LONG).show();
                gotologin();
            }
        });
    }
    public void gotologin(){
        Intent in=new Intent(this,MainActivity.class);
        startActivity(in);
    }
}